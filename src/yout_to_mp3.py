from pytube import YouTube
import os


def you_to_mp3():
    yt = YouTube(str(input('Enter URL of youtube video: \n')))
    video = yt.streams.filter(only_audio=True).first()
    destination = 'D:\\Dow\\you_music'                  # Your path
    out_file = video.download(output_path=destination)
    base, ext = os.path.splitext(out_file)
    new_file = base + '.mp3'
    os.rename(out_file, new_file)
    print(yt.title + '  has been successfully downloaded.')


def main():
    you_to_mp3()


if __name__ == '__main__':
    main()
